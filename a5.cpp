#include "tError.h"
#include <string>
#include <iostream>
#include <vector>
#include <algorithm> // http://www.cplusplus.com/reference/algorithm/find/ 
#include <array>  //http://geekwentfreak.com/posts/c/cpp_vector_of_array/
#include <cctype>
using namespace std;

class board {
	private:
		int boardsize; // board side length (default 8)
	public:
		int turnCount;
		vector< array<int, 2> > p1Pieces;
		vector< array<int, 2> > p2Pieces;

		board()
		:boardsize(8), turnCount(0)
		{
			p1Pieces.push_back({3,3});
			p1Pieces.push_back({4,4});

			p2Pieces.push_back({3,4});
			p2Pieces.push_back({4,3});
		}


		vector<array<int,2>> possibleMoves(int pTurn){
			if (pTurn == 1)
				return possibleMovesFunc(p1Pieces,p2Pieces);
			else
				return possibleMovesFunc(p2Pieces, p1Pieces);
		}


		vector<array<int,2>> possibleMovesFunc(const vector<array<int,2>>& currentPlayer, const vector<array<int, 2>>& opponent){
			vector<array<int,2>> posMoves;
			array<int, 2> home;
			array<int, 2> test;
			vector<array <int,2>>::iterator it;
			for (int i=0; i< currentPlayer.size(); i++){
				// look in 8 compass directions
				home=currentPlayer[i];
				test = home;

				//only checking valid board locations for now
				//----------------------

				//up
				test[0] = home[0]-1;
				//---
				if (test[0] >= 0){

				//up-left
				test[1] = home[1]-1;
				if (test[1] >= 0){
					if (search(opponent,test) && !search(currentPlayer,test)){ // if it finds an opponent's piece and not the currentPlayer's
						while (search(opponent,test)){	
							test[0] = test[0] - 1;
							test[1] = test[1] - 1;
						}
						
						if(test[0] >= 0 && test[1] >= 0 && !search(currentPlayer,test)){
							if (!search(posMoves,test))
								posMoves.push_back(test);
						}
					}
				}



				//up-middle
				test[0] = home[0]-1;
				test[1] = home[1];
					if (search(opponent,test) && !search(currentPlayer,test)){ 
						while (search(opponent,test)){	
							test[0] = test[0] - 1;
						}
						
						if(test[0] >= 0 && !search(currentPlayer,test)){
							if (!search(posMoves,test))
								posMoves.push_back(test);
						}
					}
	
				


				//up-right
				test[0] = home[0]-1;
				test[1] = home[1]+1;		
					if (test[1] <= 7){
					if (search(opponent,test) && !search(currentPlayer,test)){ 
						while (search(opponent,test)){	
							test[0] = test[0] - 1;
							test[1] = test[1] + 1;
						}
						
						if(test[0] >= 0 && test[1] <= 7 && !search(currentPlayer,test)){
							if (!search(posMoves,test))
								posMoves.push_back(test);
						}
					}
				}
					
				}

				//----------------------

				//mid
				test[0] = home[0];
				//---



				//mid-left
				test[1] = home[1]-1;
				if (test[1] >= 0){
					if (search(opponent,test) && !search(currentPlayer,test)){ 
						while (search(opponent,test)){	
							test[1] = test[1] - 1;
						}
						
						if(test[1] >= 0 && !search(currentPlayer,test)){
							if (!search(posMoves,test))
								posMoves.push_back(test);
						}
					}
				}
	
				
				//mid-right
				test[1] = home[1]+1;
				if (test[1]<=7){
					if (search(opponent,test) && !search(currentPlayer,test)){ 
						while (search(opponent,test)){	
							test[1] = test[1] + 1;
						}
						
						if(test[1] <= 7 && !search(currentPlayer,test)){
							if (!search(posMoves,test))
								posMoves.push_back(test);
						}
					}
				}
	
				//----------------------
				
				//down
				test[0] = home[0]+1;
				//---
				if (test[0] <= 7){



				//down-left
				test[1] = home[1]-1;
				if (test[1] >= 0){
					if (search(opponent,test) && !search(currentPlayer,test)){ 
						while (search(opponent,test)){	
							test[0] = test[0] + 1;
							test[1] = test[1] - 1;
						}
						
						if(test[0] <= 7 && test[1] >= 0 && !search(currentPlayer,test)){
							if (!search(posMoves,test))
								posMoves.push_back(test);
						}
					}
				}
		

				//down-mid
				test[0] = home[0]+1;
				test[1] = home[1];
					if (search(opponent,test) && !search(currentPlayer,test)){ 
						while (search(opponent,test)){	
							test[0] = test[0] + 1;
						}
						
						if(test[0] <= 7 && !search(currentPlayer,test)){
							if (!search(posMoves,test))
								posMoves.push_back(test);
						}
					}




				//down-right
				test[0] = home[0]+1;
				test[1] = home[1]+1;
				if (test[1] <= 7){
					if (search(opponent,test) && !search(currentPlayer,test)){ 
						while (search(opponent,test)){	
							test[0] = test[0] + 1;
							test[1] = test[1] + 1;
						}
						
						if(test[0] <= 7 && test[1] <= 7 && !search(currentPlayer,test)){
							if (!search(posMoves,test))
								posMoves.push_back(test);
						}
					}
				}
		



				}
				//----------------------
				



			}

		return posMoves;
		}


		bool search(vector< array<int,2> > v, array<int,2> a){ // check if a is in v
			vector<array<int,2>>::iterator it;
	       		it = find(v.begin(), v.end(), a);
			if (it == v.end())
				return false;
			else
				return true;
		}	       

		void boardDisplay(int n=0){
//			vector<array <int,2>>::iterator it;
			array<int, 2> x;
//			int count = 1;
			cout<<endl;
			vector<array <int,2>> posmov = possibleMoves(1);
			for (int i=0; i<boardsize;i++){
				for (int j=0; j<boardsize; j++){
					x = {i,j};
					if (search(p1Pieces,x)){
						cout<<"\033[1;32m"<<"O"<<"\033[0m";
						continue;
					}
					
					if (search(p2Pieces,x)){
						cout<<"\033[1;31m"<<"O"<<"\033[0m";
						continue;
					}

					if (search(posmov,x) && n==1){
						vector<array<int,2>>::iterator it = find(posmov.begin(), posmov.end(),x);// https://thispointer.com/c-how-to-find-an-element-in-vector-and-get-its-index/
						int index = distance(posmov.begin(), it);
						char c = 65+index;
						cout<<"\033[1;36m"<<c<<"\033[0m";
						continue;
					}

					else
						cout<<'.';
				}
				cout<<endl;
			}

				
		}
	

	void placeMoveEasy(array<int,2> move, int turn){
		if (turn == 1)
			placeMove(move,p1Pieces,p2Pieces);
		if (turn == 2)
			placeMove(move,p2Pieces,p1Pieces);

	}


	void placeMove(array<int,2> loc, vector<array<int,2>> &currentPlayer, vector<array<int,2>> &opponent){
		//same as possiblemoves, but with temp vector for each direction. add tiles in direction until not opponents tile
		//if your tile then switch the items in the temp vector from opponent's tiles to yours
		//if opponent or blank, throw the temp vecc away (empty or something idk)
			vector<array<int,2>> flipList;
			vector<array<int,2>> tempList;
			array<int, 2> home = loc;
			array<int, 2> test = loc;
			vector<array <int,2>>::iterator it;
				// look in 8 compass directions

				//only checking valid board locations for now
				//----------------------

				//up
				test[0] = home[0]-1;
				//---
				if (test[0] >= 0){

				//up-left
				test[1] = home[1]-1;
				if (test[1] >= 0){
					if (search(opponent,test) && !search(currentPlayer,test)){ // if it finds an opponent's piece and not the currentPlayer's
						while (search(opponent,test)){
							tempList.push_back(test);	
							test[0] = test[0] - 1;
							test[1] = test[1] - 1;
							
						}
						
						if(test[0] >= 0 && test[1] >= 0 && search(currentPlayer,test)){
							for (int i=0; i<tempList.size(); i++){
								if(!search(flipList,tempList[i]))
									flipList.push_back(tempList[i]);
							}								
						}
					}
				}


				tempList.clear();
				//up-middle
				test[0] = home[0]-1;
				test[1] = home[1];
					if (search(opponent,test) && !search(currentPlayer,test)){ 
						while (search(opponent,test)){	
							tempList.push_back(test);
							test[0] = test[0] - 1;
						}
						
						if(test[0] >= 0 && search(currentPlayer,test)){
							for (int i=0; i<tempList.size(); i++){
								if(!search(flipList,tempList[i]))
									flipList.push_back(tempList[i]);
							}	
						}
					}
	
				


				//up-right
				tempList.clear();
				test[0] = home[0]-1;
				test[1] = home[1]+1;		
					if (test[1] <= 7){
					if (search(opponent,test) && !search(currentPlayer,test)){ 
						while (search(opponent,test)){	
							tempList.push_back(test);
							test[0] = test[0] - 1;
							test[1] = test[1] + 1;
						}
						
						if(test[0] >= 0 && test[1] <= 7 && search(currentPlayer,test)){
							for (int i=0; i<tempList.size(); i++){
								if(!search(flipList,tempList[i]))
									flipList.push_back(tempList[i]);
							}	

						}
					}
				}
					
				}

				//----------------------

				//mid
				test[0] = home[0];
				//---



				//mid-left
				test[1] = home[1]-1;
				tempList.clear();
				if (test[1] >= 0){
					if (search(opponent,test) && !search(currentPlayer,test)){ 
						while (search(opponent,test)){	
							tempList.push_back(test);
							test[1] = test[1] - 1;
						}
						
						if(test[1] >= 0 && search(currentPlayer,test)){
							for (int i=0; i<tempList.size(); i++){
								if(!search(flipList,tempList[i]))
									flipList.push_back(tempList[i]);
										
							}
						}
					}
				}
	
				
				//mid-right
				tempList.clear();
				test[1] = home[1]+1;
				if (test[1]<=7){
					if (search(opponent,test) && !search(currentPlayer,test)){ 
						while (search(opponent,test)){
							tempList.push_back(test);	
							test[1] = test[1] + 1;
						}
						
						if(test[1] <= 7 && search(currentPlayer,test)){
							for (int i=0; i<tempList.size(); i++){
								if(!search(flipList,tempList[i]))
									flipList.push_back(tempList[i]);
							}		
						}
					}
				}
	
				//----------------------
				
				//down
				test[0] = home[0]+1;
				//---
				if (test[0] <= 7){



				//down-left
				test[1] = home[1]-1;
				tempList.clear();
				if (test[1] >= 0){
					if (search(opponent,test) && !search(currentPlayer,test)){ 
						while (search(opponent,test)){	
							tempList.push_back(test);
							test[0] = test[0] + 1;
							test[1] = test[1] - 1;
						}
						
						if(test[0] <= 7 && test[1] >= 0 && search(currentPlayer,test)){
							for (int i=0; i<tempList.size(); i++){
								if(!search(flipList,tempList[i]))
									flipList.push_back(tempList[i]);
							}	
						}
					}
				}
		

				//down-mid
				test[0] = home[0]+1;
				test[1] = home[1];
				tempList.clear();
					if (search(opponent,test) && !search(currentPlayer,test)){ 
						while (search(opponent,test)){
							tempList.push_back(test);	
							test[0] = test[0] + 1;
						}
						
						if(test[0] <= 7 && search(currentPlayer,test)){
							for (int i=0; i<tempList.size(); i++){
								if(!search(flipList,tempList[i]))
									flipList.push_back(tempList[i]);
							}	
						}
					}




				//down-right
				test[0] = home[0]+1;
				test[1] = home[1]+1;
				tempList.clear();
				if (test[1] <= 7){
					if (search(opponent,test) && !search(currentPlayer,test)){ 
						while (search(opponent,test)){
							tempList.push_back(test);	
							test[0] = test[0] + 1;
							test[1] = test[1] + 1;
						}
						
						if(test[0] <= 7 && test[1] <= 7 && search(currentPlayer,test)){
							for (int i=0; i<tempList.size(); i++){
								if(!search(flipList,tempList[i]))
									flipList.push_back(tempList[i]);
							}	
						}
					}
				}
		



				}
				//----------------------
		currentPlayer.push_back(home);
		for (int i = 0; i<flipList.size();i++){
			currentPlayer.push_back(flipList[i]);
			for (int j=0; j<opponent.size();j++){
				if (opponent[j] == flipList[i]){
					opponent.erase(opponent.begin()+j);
					break;

			}	
		}

		}



	}



};

class player{
	public:
		virtual void getMove(board &bd) = 0;

		virtual ~player(){};
};


class human_player:public player {

	public:
	
	void getMove(board &bd){
		vector<array<int,2>> posMoves = bd.possibleMovesFunc(bd.p1Pieces,bd.p2Pieces);
		char moveNum;
		cout<<endl<<"//////////////////////////////-"<<endl;
		bd.boardDisplay(1);
		cout<<"Enter your move letter (upper or lower case): ";
		try{
			cin>>moveNum;
		}
		catch(...){
			t::error("You must enter a number");
		}
		moveNum = toupper(moveNum);
		bd.placeMove(posMoves[moveNum-65],bd.p1Pieces,bd.p2Pieces);
	}
	
};

class computer_player: public player{
	public:
		void getMove(board &bd){
			bd.placeMoveEasy(bestMove(bd),2);
		}



	

		array<int,2> bestMove(board b){
			int scoreMin = 9999;
			int score = 9999;
			int turn = 2;
			int branchNum;
			vector<array<int,2>> vec = b.possibleMoves(turn); // turn 2
			for (int i = 0; i<vec.size(); i++){
				int score = bestMoveFunc(b,vec[i],1);
				if (score < scoreMin){
					branchNum = i;
					scoreMin = score;

				}
			}
		cout<<scoreMin<<endl;
		return vec[branchNum];
		}


		int bestMoveFunc(const board& bd, array<int, 2> move, int turn){
			board b = bd;
			static int count = 0;
			count++;
	//		cout<<count<<endl;
	//		cout<<b.possibleMoves(turn).size();
	//		cout<<count<<endl;
			
			int otherTurn = (turn==1) ? 2 : 1;
			array<int,2> y = {-1,-1};
			if (move != y)
				b.placeMoveEasy(move,otherTurn);
	
			if (b.possibleMoves(1).size() == 0 && b.possibleMoves(2).size() == 0 || count >= 4){
				int r = b.p1Pieces.size() - b.p2Pieces.size();
				count=count-1;
				return r;
			}
			else if (b.possibleMoves(turn).size() == 0){
//				if (turn == 1)
//					turn = 2;
//				else if (turn == 2) // else turn = 1; ?
//					turn = 1;
				array<int,2> x = {-1,-1};
				return bestMoveFunc(b,x,otherTurn);
			}
			int scoreCompare;
			if (turn == 1)
				scoreCompare = -9999;
			if (turn == 2)
				scoreCompare = 9999;

			vector<array<int,2>> vec = b.possibleMoves(turn);
			int result;
			for (int i=0; i<vec.size(); i++){
				int score = bestMoveFunc(b,vec[i], otherTurn);
				if (turn == 2){
					if (score<scoreCompare)
						scoreCompare = score;
				}

				if (turn == 1){
					if (score > scoreCompare)
						scoreCompare = score;
				}
			//	if (scoreCompare<=0) cout<<scoreCompare<<endl;
			}

			count = count - 1;	
		 	return scoreCompare;

		}
};

int main(){
computer_player c;
human_player g;
board b;
int alpha;
int beta;
int currentTurn = 1;
int lastTurn = 2;
int temp;
while (true){

alpha = (b.possibleMoves(currentTurn)).size();
beta = (b.possibleMoves(lastTurn)).size();

if (alpha == 0 && beta == 0)
	break;
b.turnCount++;

if (alpha == 0){
	temp = lastTurn;
	lastTurn = currentTurn;
	currentTurn = temp;
	continue;
}

b.boardDisplay();
cout<<"Turn Number: "<<b.turnCount<<endl;
if (currentTurn == 1){
	g.getMove(b);
	currentTurn = 2;
	lastTurn = 1;

}
else{
	c.getMove(b);
	currentTurn = 1;
	lastTurn = 2;
}}
b.boardDisplay();
int p1Score = b.p1Pieces.size();
int p2Score = b.p2Pieces.size();


cout<<"Your Score:     "<<p1Score<<endl;
cout<<"Computer Score: "<<p2Score<<endl;
if (p1Score > p2Score)
	cout<<"You win!"<<endl;
if (p2Score > p1Score)
	cout<<"You lose..."<<endl;
if (p1Score == p2Score)
	cout<<"It's a tie!"<<endl;
/*
human currentPlayer1();
computer currentPlayer2();

while (true){
	if (b.possibleMoves(b.currentPlayerTurn).size() == 0){
		b.winMessage();
		break;
	}

	if (b.currentPlayerTurn == 1){
		currentPlayer1.playMove(b);
	}

	if (b.currentPlayerTurn == 2){
		currentPlayer2.playMove(b);
	}

	b.currentPlayerTurn = (b.currentPlayerTurn == 1)? 2: 1; // change currentPlayer


*/
}



