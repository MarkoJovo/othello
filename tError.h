#ifndef CMPT_ERROR_H
#define CMPT_ERROR_H

#include <string>
#include <stdexcept>

namespace t {

inline void error(const std::string& message)
{
    throw std::runtime_error(message);
}

}
#endif
